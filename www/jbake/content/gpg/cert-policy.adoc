= Richtlinien zur Signatur von OpenPGP Schlüsseln 
DebXWoody
2018-10-26
:jbake-type: page
:jbake-status: draft 
:jbake-tags: openpgp
:idprefix:
[abstract]
Dieses Dokument beschreibt die Richtlinien für die 
Unterzeichnung von Schlüsseln.

== Allgemeine Informationen
Das signieren von öffentlichen OpenPGP Schlüsseln wird verwenden um
die Korrektheit und Gültigkeit eines Schlüsseln zu bestätigen.
Zusammen mit dem Vertrauen in einer Person bildet es die Grundlage im
Web of Trust, welches für die Prüfung von Signaturen verwendet werden
kann.

== Level der Zertifizierung
Das unterschreiben eines Schlüsseln zertifiziert den unterschrieben
Schlüssel auf Korrektheit und Gültigkeit. Das Zertifikat dient anderen
Personen dazu, zu entscheiden ob man durch die Zertifizierung anderer
Personen dem zertifizierten Schlüssel vertraut oder nicht vertraut.

Die Stufen (Level) der Zertifikate werden in dieser Richtlinie wie
folgt definiert.

=== sig	- Überprüfungsgrad 0 (unbekannt)
Die Korrektheit und Gültigkeit des Schlüssels ist unbekannt. Durch die
Signatur der Stufe 0 wurde weder geprüft ob der Schlüssel wirklich der
Person ist, welche in der Benutzer-IDs des Schlüssels hinterlegt wurde
weder wurde der Fingerprint geprüft.

Die Information hinter dem sig sollte nicht verwenden werden, um einen
Schlüssel als Gültig zu betrachten. Die Signatur sagt ausschließlich
aus: `Ich habe den Schlüssel wahrgenommen, kenne jedoch weder den
Besitzer noch weiß ich ob der Schlüssel richtig oder falsch ist`.

===  sig 1-	Überprüfungsgrad 1 (nicht geprüft)
Eine Signatur mit dem Level 1 wird verwenden, wenn man von der
Gültigkeit eines Schlüssels ausgeht, die Korrektheit jedoch nicht
prüfen könnte. Streng genommen unterscheidet sich diese Stufe nur sehr
gering von der Stufe 0 vom Aspekt des Vertrauens auf die Gültigkeit
eines Schlüssels. Man kann auch in dieser Stufe *nicht* davon ausgehen,
dass der Schlüssel korrekt ist. Wir denke der Schlüssel ist Gültig,
wissen es aber nicht.

=== sig 2 - Überprüfungsgrad 2 (flüchtig)
Diese Stufe kann Auswirkungen auf die Gültigkeit von Schlüsseln habe,
welche im Kontext des Web of Trust verwendet verwendet werden. D.h.
habe beispielsweise drei Personen einen Schlüssel mit cert Level 2
unterzeichnet, kann es dazuführen dass andere Personen diesen
Schlüssel aufgrund dem Web of Trust als Gültig ansehen.

=== sig 3 - Überprüfungsgrad 3 (sorgfältig)
Eine Signatur von level 3 erfolgt nur dann wenn die folgenden Punkte
persönlich geprüft wurden und korrekt sind:

* User-ID des Schlüssels stimmt mit vollkommen mit dem im
  Personalausweis überein.
* Die Person stimmt mit dem Lichtbildausweis auf dem Person überein.
* Der Fingerprint wurde von beiden geprüft und bestätigt
* Die Schlüssellänge, Typ, Erstellungsdatum und Verfallsdatum stimmen
  überein.

== OpenPGP Smart Card
Um sicherzustellen, dass keine Weitere Person Zugriff auf den privaten
Schlüssel bekommt, kann eine OpenPGP Smart Card verwendet werden. Dies
ist auch in dieser Richtlinie so vorgesehen.

== Zusammenfassung
Wenn du dich an die oben beschriebenen Richtlinien hältst:

* Verwenden der Richtigen cert-level sig, sig1, sig2, sig3
* Dein Schlüssel ist auf einer OpenPGP Smart Card o.ä.

dann kannst du deine Signatur wie folgt machen:

	gpg --cert-policy-url <URL>



