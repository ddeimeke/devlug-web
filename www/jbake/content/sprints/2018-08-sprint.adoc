= vLUG-Sprint 2018-08
DebXWoody
2018-08-01
:jbake-type: sprint 
:jbake-status: published
:jbake-tags: devlug, sprint
:vsprint-name: vLUG-Sprint 2018-08
:vsprint-time-slot: 01.08.2018 - 30.09.2018
:vsprint-url-milestone: https://gitlab.com/groups/devlug/-/milestones/2
:idprefix:
:toc:
[abstract]
Dies ist der Artikel über den {vsprint-name}. 

== Zeitraum
Gesamt:: {vsprint-time-slot}

== Themen

link:{vsprint-url-milestone}[{vsprint-name}]

.Themen 
|===
|Beschreibung | Issue | Link

| Erweitern unseres IRC Bot - devTuxBot 
| link:https://gitlab.com/devlug/devel/devTuxBot/issues/1[devel devTuxBot #1]
| link:https://www.devlug.de/members/devTuxBot.html[Memberseite] 

| devLUG-Session: taskwarrior und timewarrior
| link:https://gitlab.com/devlug/planung-und-ideen/issues/27[planung-und-udeen #27]
|

| Syntax und Konfiguration in vim (notes)
| link:https://gitlab.com/devlug/devlug-sandbox/issues/1[devlug-sandbox #1]
| link:https://gitlab.com/devlug/devlug-sandbox/tree/master/vim/notes[GitLab]

|===


