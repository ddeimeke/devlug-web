= ddeimeke
ddeimeke
2019-01-31
:jbake-type: member
:jbake-status: published
:jbake-tags: member
:jbake-updated: 2019-01-31
:jbake-membernamegitlab: ddeimeke
:jbake-membernameirc: ddeimeke
:jbake-memberurlmastodon: https://friend.d5e.org/profile/dirk
:jbake-membergpgid: 0xD8E83C80
:jbake-membergpgfingerprint: E80C E8A5 8073 73E6 4766 66E2 EAD6 49C8 D8E8 3C80
:jbake-memberdistribution: Fedora, CentOS, beruflich RHEL und Solaris
:jbake-memberlinuxwindowmanager: KWin
:jbake-memberlinuxsprachen: Bash, Python, Perl, ...
:jbake-memberlinuxseit: 1996
:jbake-memberdevlugseit: 2018
:jbake-memberdevluggroup: 
:idprefix:
:sectanchors:
:sectlinks:

Systemadministrator und Taskwarrior