= Dokumente der devLUG 
DebXWoody
2018-05-01
:jbake-type: devlug
:jbake-status: published 
:jbake-tags: dokumente 
:jbake-updated: 2018-06-17
:idprefix:
:toc:
:sectanchors:
:sectlinks:

[abstract]
Wir haben auf unserer Homepage mehrere Dokumente. Dieser Artikel beschreibt wie du Einträge ergänzen oder selber welche schreiben kannst.

== Einleitung
Die meisten Dokumente liegen im asciidoc-Format vor und sind in einem Git-Repository gespeichert.
Mit dem Programm jbake werden die Dokument in HTML-Dateien umgewandelt.

Weitere Informationen:

* http://asciidoctor.org/docs/asciidoc-syntax-quick-reference/
* https://jbake.org/
* https://git-scm.com/

== Dokumente
=== Arten von Dokumenten
Wir haben verschiedene Arten von Dokumenten auf unsere Homepage.

* devLUG Dokumente `*devlug*` - Dies sind Dokumente über die devLUG, sie beinhalten Informationen über unsere Linux User Group.
* Projekt Dokumente `*project*` - Dies sind Dokumente über ein Projekt, welches wir in der devLUG haben.
* Artikel `*article*` - Artikel sind Dokumente die etwas über ein bestimmtes Thema berichten.
* Referenzkarten - Referenzkarten sind Zusammenfassungen mit wichtigen Infos zu einem Thema.
* Post `*post*` - Ankündigungen oder News

=== Wo finde ich welche Dokumente
Alle Dokumente (Blog-Post) werden auf unserer Homepage im Archive angezeigt:
https://www.devlug.de/archive.html

Dokumente der devLUG haben den Tag `*devlug*` und eine Liste der Dokumente findest du hier:
https://www.devlug.de/devlug/devlug.html

Artikel haben den Tag `*artikel*` und eine Liste der Dokumente findest du hier:
https://www.devlug.de/articles.html

Referenzkarten haben den Tag `*refcard*` und eine Liste der Dokumente findest du hier:
https://www.devlug.de/tags/refcard.html

== Mitmachen
Jeder kann mitmachen. Es gibt nur wenig Punkte auf die du achten solltest:

* Dokumente mit dem Tag `*devlug*` sollten vorher abgestimmt werden (Mailingliste bzw. Stefan und Frank)

Du solltest ein paar Grundlagen von git und asciidoc kennen.

Informationen zu asciidoctor sind hier: http://asciidoctor.org/docs/asciidoc-syntax-quick-reference/

Solltest du noch fragen haben, dann kannst du einfach bei uns auf der Mailingliste oder im IRC nachfragen.

Um bei den ersten Schritten zu helfen, haben wir ein
link:https://www.devlug.de/devlug/contributionguide.html[Contribution Guide]
geschrieben.

=== Homepage auf gitlab
Wenn du noch kein Account auf gitlab hast, dann solltest du dir einen anlegen: https://gitlab.com

Die Dokumente liegen im Repository unserer Homepage auf https://gitlab.com/devlug/devlug-web. Hier kannst du
über "Fork" eine "Kopie" in deinem User erstellen.

Hast du dies gemacht, so hast du die Homepage und alle Dokumente in deinem user:

	https://gitlab.com/<DeinUserID>/devlug-web

Auf deinem PC erstellst du dir jetzt dein Repository:

	git clone git@gitlab.com:<DeinUserID>/devlug-web.git

Git zieht sich dann unserer Homepage von gitlab auf deinen Computer. Du hast dann alle Dateien auf deinem Rechner.

Die Dokumente liegen alle im Verzeichnis `www/jbake/content/`.
 
=== Dokumente bearbeiten
Lokal können die Artikel bearbeitet werden. Die Dokumente sind in der Regel im asciidoc-Format und Ende mit .adoc.
Informationen zur Syntax sind auf der Homepage von
link:http://asciidoctor.org/docs/asciidoc-syntax-quick-reference/[asciidoctor] zu finden.

Vorlagen für die verschiedenen Dokumente sind im Verzeichnis link:https://gitlab.com/devlug/devlug-web/tree/master/template[template].

Um die Homepage dann von adoc in HTML-Dateien zumzuwandeln verwenden wird link:https://jbake.org/[jbake].

Mehr Informationen findest du in unserer README.adoc Datei.

=== Dokument einspielen und request erstellen
Damit deine Änderungen auf unserer Homepage kommen, müssen die Änderungen zunächst bei dir im Account eingespielt werden
und dann per Request an uns weitergeleitet werden.

Beispiel, du hast eine Korrektur im Dokument Hallo.adoc gemacht.

	git add www/jbake/content/article/Hallo.adoc

	git commit

	git push


Merge Request stellen

Auf der Homepage von GitLab dein Projekt auswählen:

	https://gitlab.com/<DeinUserID>/devlug-web

Oben auf das + Symbol und dann "New merge request". Links wählst du deine Branch aus und rechts wo es hin soll.

https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html



